import { defineClientAppEnhance } from "@vuepress/client";
import NpmLink from "./components/NpmLink.vue";
import Badges from "./components/Badges.vue";
import Project from "./components/Project.vue";

export default defineClientAppEnhance(({ app}) => {
  app.component("NpmLink", NpmLink);
  app.component("Badges", Badges)
  app.component("Project", Project)
});
