import { defaultTheme, defineUserConfig, viteBundler } from "vuepress";

export default defineUserConfig({
  bundler: viteBundler({
    vuePluginOptions: {
      template: {
        compilerOptions: {
          isCustomElement: (tag) => tag === "center",
        },
      },
    },
  }),
  base: "/",
  title: "Gachou",
  head: [["link", { rel: "icon", href: "/favicon.svg" }]],
  theme: defaultTheme({
    logo: "/favicon.svg",
    sidebar: [
      "/getting-started",
      "/architecture/backend",
      "/architecture/frontend",
      "/contributing",
    ],
  }),
});
