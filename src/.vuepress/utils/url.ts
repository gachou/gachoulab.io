
export function url(template, ...subst) {
  return String.raw(template, ...subst.map(encodeURIComponent))
}
