# Backend-Architecture

In principle, the architecture should be clean as
[described by Robert C. Martin](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html).

At the moment, we plan the following components, but this diagram may not be
complete or fully implemented yet.

![Clean Architecture](./clean-architecture.svg)

## Domain model

These are the base entities used by Gachou. There is no business logic here,
only data-structures.

## Core

The core module contains the basic business logic and if connects the other
modules to each other. The outer parts of the circle are interfaces that belong
to the core module. These are implemented by the adjacent components.

## Use cases

Code for use-cases like "file upload", "list files", "tag file" etc.

## HTTP API

Uses core module to provide a rest-api for http clients and frontends

## File storage

Implements the Storage API defined in Core to provide access to file storage
mechanisms

## Metadata-DB

Implements the Metadata API defined in Core to provide access to metadata
storage and search facilities.

## Metadata Extractor

Implements the Metadata Extractor API defined in Core to provide mechanisms for
extracting and storing metadata into media files

## Transcoding

Implements the Transcoding API defined in Core to provide mechanisms for
transforming files between media formats and sizes

# Clean architecture rules

Dependencies may only go from outside to the inside. Core may not use classes or
interfaces defined in use-cases etc.
