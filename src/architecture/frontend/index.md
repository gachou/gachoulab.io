# Frontend architecture

![](./frontend.svg)

## Core Model

The core model consists of interfaces used by the application and the Preact
components.

## API functions

To access the backend, we define functions that accept and return interfaces
from the core model. The functions themselves use an openapi-client that is
auto-generated from the openapi-spec of the backend, for example via
<npm-link name="openapi-client-axios"/> or <npm-link name="oazapfts" />.

## Websocket connection

The websocket connection is used for user notifications. It writes data to the
recoil store from there it is distributed to the components that display them.

## Recoil

For global parts of the application states (such as "user info" and
"notifications"), we use a Recoil store (<npm-link name="recoil" />).

Page state and component state is kept within the components.

## Atomic design

We use [atomic design](https://bradfrost.com/blog/post/atomic-web-design/) for
structuring components.

## Further tools

- <npm-link name="eslint"/> is used for static analysis.
- <npm-link name="dependency-cruiser"/> is used to enforce architectural
  decisions.
- <npm-link name="prettier"/> is used for code formatting.
- <npm-link name="msw"/> is used to mock backends for testing and storybook
- <npm-link name="storybook"/> is used to showcase single components and
  possible for visual testing
