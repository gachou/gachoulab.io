# [Backend](backend)

We use [Quarkus](https://quarkus.io/) and Java as backend technologies.

- the IDE support and dev-tools for Java is much better than for Node
- I am familiar with Java and I want to get things done.
- My server runs on low resources, and I don't want to waste them.

# [Frontend](frontend)

We use Preact-CLI to create a frontend.

- I haven't used Preact yet, but it seems to be a solid framework.
- I am familiar with React, which should help getting things done here.
