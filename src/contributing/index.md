# Contributing

# Contributing

Gachou is hosted and developed at
[https://gitlab.com/gachou](https://gitlab.com/gachou)

## Planning / Issues

- [Kanban board](https://gitlab.com/gachou/gachou/-/boards/4316918) - Current
  tasks (ready, in-progress, in-review, closed)
- [Backlog](https://gitlab.com/gachou/gachou/-/boards/4316927) - No-yet-ready
  tasks

## Code

The following repositories are relevant. More contributing information can be
found in the repositories' README.md files.

<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Status</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
      <tr>
        <td><Project name="gachou" /></td>
        <td><Badges project="gachou" /></td>
        <td>Mono-Repo containing backend, frontend and e2e-tests</td>
      </tr>
      <tr>
        <td><Project name="gachou.gitlab.io" /></td>
        <td><Badges project="gachou.gitlab.io" /></td>
        <td>this website</td>
      </tr>
      <tr>
        <td><Project name="artwork" /></td>
        <td></td>
        <td>some pictures and logos</td>
      </tr>
      <tr>
        <td><Project name="testdata" /></td>
        <td><Badges project="testdata" branch="master" /></td>
        <td>image- and video-files for testing transcoding, thumbnail-generation etc.</td>
      </tr>
    </tbody>
</table>

I usually work on Gachou on fridays. If you want to get in touch, send me a mail
to `npm@knappi.org`. It might take a while until I respond though, I get a lot
of spam on that address.
