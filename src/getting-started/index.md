# Getting started

::: warning

You will only see a login page, and you will be able to log in, but there is
nothing more yet.

Everything is in **very** early stages of development.

:::

1. Create a file `docker-compose.yml` with the following content
   @[code](./docker-compose/docker-compose.yml)

2. Add a file `.env` with the following content to the same directory
   @[code](./docker-compose/.env)

3. Run

   ```bash
   docker-compose up -d
   ```

4. Open [http://localhost:9080](http://localhost:9080) with your browser.

## Create an admin user

The first thing you will see is a dialog that notifies you of the missing admin
user. Enter a password in the password field and click "Encrypt password".

![](../screenshots/authentication/create-admin-user.spec.ts/create-admin-user-modal.png)

What you see is a the password in encrypted form, in different variants ready to
be used. For `docker-compose` we need the "Environment variable".

Click the input field to copy the value to the clipboard, and insert it into
your `.env` file.

Run `docker-compose up -d` again. After a couple of seconds, the dialog
disappears, and you are able to log in with the user `admin` and the password
that you just entered.

## Running on a remote server

If you want to run Gachou on a remote server, you have to adjust the url- and
port-values in the `.env` file to match the reality.

If you run it in a public network you should make sure that you have a reverse
proxy configured for HTTPS. Currently, there is no official way to configure
HTTPS directly, although the setup is using [Quarkus](https://quarkus.io/) and
[Caddy](https://caddyserver.com/), which should both support it.

## User-Management

Gachou comes with a very simple user management page, that allows you to add and
remove users, and change passwords.

Only the "admin" user is allowed to manage users. On http://localhost:3000/users
you will see a list of all users.

![List of users](../screenshots/users/create-add-remove-users.spec.ts/users-list.png)

- Click the "Delete" button to delete the corresponding used. **Warning: There
  is no confirmation dialog yet.**
- Click the "Change password" button to set a new password for the user
- Click the "Add user" button at the bottom to creae a new user

### Adding users

When you click the "Add user" button will see the following screen:

![Add user](../screenshots/users/create-add-remove-users.spec.ts/create-user-page.png)

Fill-out the fields and click "Create user" to create a user. You will see an
error message, if the username already exists or if the password confirmation
does not match the password.

### Changing passwords

The form to change password is similar, but the username cannot be changed in
this view.

![Change passwords](../screenshots/users/create-add-remove-users.spec.ts/edit-user-page.png)
