---
lang: en-US
home: true
heroTitle: Gachou
heroText: Gachou 画帳
tagline: Your picture album
heroImage: "/gachou-logo.svg"
actions:
  - text: Get started
    link: /getting-started/
    type: secondary
  - text: Contributing
    link: /contributing/
    type: secondary
  - text: Demo
    link: https://gachou-demo.knappi.org
    type: primary
---

# About Gachou

Since 2014 (probably longer) I had the idea of building an intranet tool for
managing family photos and videos. I was using
[digikam](https://www.digikam.org/) at that time, which is a great tool albeit
with restrictions

- it only runs on one computer (no network, multi-user access)
- videos were not or hardly supported
- on the other hand, it has a lot of features that I don't need.

I just want to manage my files, tag them, delete some of them... Find them.
That's all

---

I had a look around for other tools, but didn't find anything that fulfilled my
requirements. Then I started building something. A project that started and
stopped multiple times.

At the moment, there is an early version of Gachou running on the server in my
basement. But when I tried to refactor things, I noticed how hard that had
become.

So here comes the big rewrite, and this time, "everything will be better (tm)"

- Clean architecture
- Java-Backend (Quarkus) instead of Node.js
- Frontend in React, with modern tools
- Target to run in docker-compose
- Should also run in a Serverless environment
- Some feature planning
- Documentation and blog posts, maybe videos about the progress

::: warning

This is a one-man free-time project. Don't expect fast progress.

:::
